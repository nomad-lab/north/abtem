# recipe to create the nomad-remote-tools-hub ewr_abinit_em_webtop container via a dockerfile
FROM gitlab-registry.mpcdf.mpg.de/nomad-lab/nomad-remote-tools-hub/webtop:v0.0.1

# for testing the container locally without running in oauth errors
# FROM ghcr.io/linuxserver/webtop:amd64-ubuntu-openbox-version-e1079163
# FROM ghcr.io/linuxserver/webtop:amd64-ubuntu-xfce-version-c603dc47
# start of borrowed from gitlabmpcdf customized webtop
# ENV CUSTOM_PORT=8888
# ENV PUID=1000
# ENV PGID=1000
# ports and volumes
# EXPOSE 8888
# VOLUME /config
# end of borrowed from north webtop

USER root

# ### set the environment already, knowing where miniconda will be installed
ENV PATH=/usr/local/miniconda3/bin:$PATH

RUN mkdir -p /home \
  && mkdir -p /home/vesta \
  && mkdir -p /home/gpaw \
  && mkdir -p /home/abtem

COPY Cheatsheet.ipynb FAIRmatNewLogo.png NOMADOasisLogo.png /home/
COPY 02-exec-cmd /config/custom-cont-init.d/02-exec-cmd

# install operating system dependencies
RUN apt update \
  && apt-get install -y git cmake unzip wget libgtk-3-dev build-essential libglu1-mesa-dev=9.0.1-1build1 libblas-dev liblapack-dev libxc-dev \
  && wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
  && chmod +x Miniconda3-latest-Linux-x86_64.sh \
  && bash ./Miniconda3-latest-Linux-x86_64.sh -b -p /usr/local/miniconda3 \
  && rm -f Miniconda3-latest-Linux-x86_64.sh \
  && cd /home \
  && conda config --add channels conda-forge \
  && conda install python jupyter jupyterlab jupyterlab-h5web gpaw abtem \
  && conda clean -afy \
  && python -m pip install --upgrade pip \
  && cd /home/vesta \
  && wget https://jp-minerals.org/vesta/archives/3.5.8/VESTA-gtk3.tar.bz2 \
  && tar -xvf VESTA-gtk3.tar.bz2 \
  && rm -f VESTA-gtk3.tar.bz2 \
  && chown -R ${PUID}:${PGID} /usr/local/miniconda3 \
  && chown -R ${PUID}:${PGID} /home

WORKDIR /home


# comments to ignore
# how to call this container for development purposes for example
# docker run -p 3000:8888 ewr-abinit-em-webtop
# && conda config --set channel_priority strict \
